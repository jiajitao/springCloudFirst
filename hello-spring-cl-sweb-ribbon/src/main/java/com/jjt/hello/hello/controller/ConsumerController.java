package com.jjt.hello.hello.controller;

import com.jjt.hello.hello.Service.HelloConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {

    //这里注入的restTemplate就是在com.sam.ConsumerApp中通过@Bean配置的实例
    @Autowired
    private HelloConsumerService helloConsumerService;

    @RequestMapping("/hello-consumer")
    public String helloConsumer() {
        return helloConsumerService.getHelloContent();
    }
}
