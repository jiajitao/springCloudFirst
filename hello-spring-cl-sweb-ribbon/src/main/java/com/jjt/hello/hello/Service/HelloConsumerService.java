package com.jjt.hello.hello.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloConsumerService {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "serviceFailure")
    public String getHelloContent() {
        return restTemplate.getForObject("http://hello-spring-cl-service/hi?msg=1111111111111", String.class);
    }

    /**
     *
     * @HystrixCommand注解定义了一个断路器，它封装了getHelloContant()方法， 当它访问的hello-spring-cl-service失败达到阀值后，
     * 将不会再调用hello-spring-cl-service， 取而代之的是返回由fallbackMethod定义的方法serviceFailure（）。
     * @HystrixCommand注解定义的fallbackMethod方法，需要特别注意的有两点：

    第一，  fallbackMethod的返回值和参数类型需要和被@HystrixCommand注解的方法完全一致。否则会在运行时抛出异常。比如本例中，
    serviceFailure（）的返回值和getHelloContant()方法的返回值都是String。

    第二，  当底层服务失败后，fallbackMethod替换的不是整个被@HystrixCommand注解的方法（本例中的getHelloContant),
    替换的只是通过restTemplate去访问的具体服务。可以从中的system输出看到， 即使失败，控制台输出里面依然会有“call SERVICE-HELLOWORLD”。

    启动eureka服务，只启动两个Helloworld服务，然后中断其中一个（模拟其中一个微服务挂起），
    访问http://localhost:8901/然后刷新, 由于有负载均衡可以看到以下两个页面交替出现。可以看到第二个被挂起的服务，
    被定义在Ribbon应该里面的错误处理方法替换了。

     */

    public String serviceFailure() {
        return "http://hello-spring-cl-service熔断机制 hello world service is not available !";
    }
}
