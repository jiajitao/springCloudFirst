package com.jjt.hello.hello.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Component
public class FeignFallBackProvider implements  FallbackProvider {
/*
* zuul集成了hystrix，也可以实现fallback，实现fallback时需要实现 FallbackProvider（springboot2.0）
* */
  @Override
    public String getRoute() {
        //return "hello-spring-cl-sumer-feign";//服务id，可以用* 或者 null 代表所有服务都过滤
      return null;

  }

    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                /**
                 * 网关向api服务请求是失败了，但是消费者客户端向网关发起的请求是OK的，
                 * 不应该把api的404,500等问题抛给客户端
                 * 网关和api服务集群对于客户端来说是黑盒子
               **/
                 return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.OK.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.OK.getReasonPhrase();
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() throws IOException {
                Map<String ,Object> map=new HashMap<String , Object>();
                map.put("status","200");
                map.put("message","无法连接，请检测您的网络刷新重试！！");
                ObjectMapper objectMapper=new ObjectMapper();
                return new ByteArrayInputStream(objectMapper.writeValueAsString(map).getBytes("utf-8"));
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                //和body中的内容编码一致，否则容易乱码
                headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
                return headers;
            }
        };
    }
}
