package com.jjt.hello.hello.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hello-spring-cl-service",fallback = HelloWorldServiceFailure.class)
public interface HelloService {

    @RequestMapping(method = RequestMethod.GET, value = "hi")
    public String sayHi(@RequestParam(value = "msg") String msg);


}