package com.jjt.hello.hello.service;

import org.springframework.stereotype.Component;

@Component
public class HelloWorldServiceFailure implements HelloService {

    @Override
    public String sayHi(String msg) {
        return "hello world service is not available !Fegin熔断机制";
    }
}
