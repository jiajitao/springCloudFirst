package com.jjt.hello.hello.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigRefreshController {

    @Value("${myself.feign.config.value}")
    private String configValue;
    /*@RefreshScope  这个类注解必须引用
    *否则
    * @Value("${myself.feign.config.value}")
    * 无法获取到最新的值
    * */
    @RequestMapping(value = "/configvalue")
    public String configvalue() {
        return "配置文件的myself.feign.config.value值为"+configValue;
    }
}
