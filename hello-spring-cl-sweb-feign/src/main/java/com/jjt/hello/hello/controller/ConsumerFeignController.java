package com.jjt.hello.hello.controller;

import com.jjt.hello.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerFeignController {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "/hello-consumer",method = RequestMethod.GET)
    public String helloConsumer(@RequestParam String msg) {
        //调用hello-service服务，注意这里用的是服务名，而不是具体的ip+port
        ///String a=restTemplate.getForObject("http://hello-spring-cl-service/hi?msg=11", String.class);
        return helloService.sayHi(msg);
    }
}
